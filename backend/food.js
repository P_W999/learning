var url = require('url');
var qs = require('querystring');

function getCollection(db) {
    return db.collection('food');
}

getFoodList = function getFoodList(req, db, onData, onEnd, onError) {
    getCollection(db).find().each(function(err, list) {
        if (!err) {
            if (list) {
                onData(list);       
            } else {
                onEnd();
            }
        } else {
            onError(err);
        }
    });
}



exports.getFoodList = getFoodList;

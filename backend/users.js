var url = require('url');
var qs = require('querystring');

/**
* Returns the users collection.
*/
function getCollection(db) {
    return db.collection('users');
}

/**
* Returns all active users/
*/
getUsers = function getUsers(req, db, onRow, onEnd, onError) {
    try {
        getCollection(db).find({active: true}).each(function(err, item) {
            if (!err) {
                if (item) {
                    onRow(item);    
                } else {
                    onEnd();
                }
            } else {
                onError(err);   
            }
        });
    } catch (err) {
        error(err);   
    }
}

/**
* Returns a single user by it's name. Expected the 'name' request paramter to be set.
*/
getUserByName = function getUserByName(req, db, onUser, onError) {
    urlParts = url.parse(req.url);
    query = qs.parse(urlParts.query);
    retreiveUserFromDb(query.name, db, onUser, onError);
}

/**
* Retreives a single user from the database. An HTTP 404 code will be passed to onError if user not found.
*/
function retreiveUserFromDb(username, db, onUser, onError) {
    getCollection(db).findOne({active: true, name: username}, function(err, doc) {
       if (!err) {
            if (doc) {
                onUser(doc);    
            } else {
                onError({code: 404, message : "user.not.found", params: {name : username}});
            }
       } else {
           onError(err);
       }
    });
}

/**
* Provides basic authentication. 'Authorization' should be set in the HTTP request header and contain base64(<user>:sha1(<password>)).
*/
authenticate = function authenticate(req, db, onSuccess, onFailure) {
    var header=req.headers['Authorization']||'',        // get the header
          token=header.split(/\s+/).pop()||'',            // and the encoded auth token
          auth=new Buffer(token, 'base64').toString(),    // convert from base64
          parts=auth.split(/:/),                          // split on colon
          username=parts[0],
          password=parts[1];
    
    retreiveUserFromDb(username, db, function(user) {
        if (user.password === password) {
            onSuccess();   
        } else {
            onFailure({code: 401, message : 'password.mismatch'});
        }
    }, function(err) {
        onFailure(err);
    });
}

/**
* Adds a user to the DB
*/ 
addUser = function addUser(req, db, onSuccess, onError) {
    var body = '';
    req.on('data', function (data) {
            body += data;
            if (body.length > 1e6) {
                req.connection.destroy();
            }
        });
    req.on('end', function () {
        body = JSON.parse(body);
        retreiveUserFromDb(body.name, db, function(user) {
            onError({code : 409, message : 'user.creation.duplicate'});   
        }, function(err) {
            if (err.code && err.code === 404) {
                body.orders = [];
                body.deposits = [];
                body.active = [];
                
                getCollection(db).insert(body, function(err, r) {
                    if (!err) {
                        if (!r) {
                            onError({code : 500, message: 'user.creation.failed'});   
                        } else {
                            onSuccess(r);   
                        }
                    } else {
                        onError(err);
                    }
                });
            } else {
                onErrro(err);   
            }
        });
    });
}

/**
* Makes a deposit for a single user.
*/ 
addMoney = function(req, db, onSuccess, onError) {
    var body = '';
    req.on('data', function (data) {
            body += data;
            if (body.length > 1e6) {
                req.connection.destroy();
            }
        });
    req.on('end', function () {
        body = JSON.parse(body);
        body.deposit.timestamp = new Date();
        getCollection(db).update({name : body.name}, {$push: { deposits : body.deposit}}, function(err, deposit) {
            if (!err) {
                onSuccess(deposit);
            } else {
                onError(err);   
            }
        });
    });
}

/**
* Adds an order for a user.
*/
addOrder = function(req, db, onSuccess, onError) {
    var body = '';
    req.on('data', function (data) {
            body += data;
            if (body.length > 1e6) {
                req.connection.destroy();
            }
        });
    req.on('end', function () {
        body = JSON.parse(body);
        body.orders.timestamp = new Date();
        getCollection(db).updateOne({name : body.name}, {$push: { orders : body.orders}}, function(err, order) {
            if (!err) {
                onSuccess(order);
            } else {
                onError(err);   
            }
        });
    });
}

exports.getUsers = getUsers;
exports.getUserByName = getUserByName;
exports.authenticate = authenticate;
exports.addUser = addUser;
exports.addMoney = addMoney;
exports.addOrder = addOrder;
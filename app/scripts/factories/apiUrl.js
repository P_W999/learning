angular.module('learningApp')
.factory('apiUrl', [function() {
    return function(path) {
        console.info(path);   
        return "/api/" + path;
    }
}]);
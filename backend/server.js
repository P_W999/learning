var http = require('http');
var url = require('url');
var qs = require('querystring');

var mongo = require('mongodb');
var client = mongo.MongoClient;
var db = null;

var users = require('./users.js');
var food = require('./food.js');

var domain = require('domain');


http.createServer(function (req, res) {
    // Create per request domain to handle unhandled exceptions
    d = domain.create();
    d.add(req);
    d.add(res);

    d.on('error', function(err) {
        console.error(err.stack);
        try {
            res.writeHead(500, {'Content-Type': "application/json"});
            res.end(err.message);   
        } catch (err2) {
            console.error("Could not send http 500");
            console.error(err2.stack);
        }
    });
    
    d.run(function() {
        handleRequest(req, res);
    });
        
}).listen(9001, '127.0.0.1');
console.log('Server running at http://127.0.0.1:9001/');

// Connect to MongoDB
client.connect("mongodb://localhost:27017/hoagie", function(err, database) {
	if (!err) {
		console.log("Connection to mongo db successfull");
        db = database;
	} else {
		throw err;
	}
});

// Actual request handler
function handleRequest(req, res) {
    var urlParts = url.parse(req.url);
    console.info('Incomming ' + req.url);
    switch (urlParts.pathname) {
        case '/':
            res.writeHead(200, {'Content-Type': "text/plain"});
            res.end('I\'m still here\n');
            break;
        case '/users':
            users.getUsers(req, db, 
                function(data) {
                    writeArray(res, data);
                }, function() {
                    endArray(res);
                }, function(err) {
                    onError(res, err);
                }
            );
            break;
        case '/users/byname':
            users.getUserByName(req, db, function(user) {
                writeObject(res, user);
            }, function(err) {
                onError(res, err); 
            });
            break;
        case '/users/add': 
            users.addUser(req, db, function(r) {
                writeObject(res, r);
            }, function(err) {
                onError(res, err);  
            });
            break;
        case '/users/deposit':
            users.addMoney(req, db, function(r) {
                writeObject(res, r);
            }, function(err) {
                onError(res, err);  
            });
            break;
        case '/orders/order':
            users.addOrder(req, db, function(r) {
                writeObject(res, r);
            }, function(err) {
                onError(res, err);  
            });
            break;
        case '/food': 
            food.getFoodList(req, db, function(data) {
                writeArray(res, data);
            }, function() {
                endArray(res);
            }, function(err) {
                onError(res, err);  
            });
            break;
        default:
            res.writeHead(404, {'Content-Type': "text/plain"});
            res.end('Not found :( ');
            break;	
    }
}

/**
* Writes a JSON object to the response with an HTTP 200 status code
*/
function writeObject(res, object) {
    delete object.password;
    
    res.statusCode = 200;
    res.setHeader('Content-Type', "application/json");
    res.end(JSON.stringify(object));
}

/**
* Writes a single element of an array to the response. Use endArray function to finialize the array
*/ 
function writeArray(res, data) {
    delete data.password;
    
    if (!res.headersSent) {
        res.statusCode = 200;
        res.setHeader('Content-Type', "application/json");
        res.write('{ "data" : [');
    } else {
        res.write(',');   
    }
    res.write(JSON.stringify(data));
}

/** 
* Ends an array (if needed, sets header to HTTP 200)
*/ 
function endArray(res) {
    if (!res.headersSent) {
        res.statusCode = 200;
        res.setHeader('Content-Type', "application/json");
        res.write('{ "data" : [');
    }
    res.end(']}');   
}

/**
* Handles error objects. An object {code : <httpcode>, message : <error_message>} can be used to specify the HTTP status code
* and optionally provide a message for the front-end to translate.
*/
function onError(res, err) {
    code = err.code;
    if (code == null || code == 'undefined' || code == 0) {
        code = 500;   
    }
    res.writeHead(code, {'Content-Type': "application/json"});
    res.end(JSON.stringify(err));
}
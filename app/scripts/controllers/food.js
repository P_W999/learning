angular.module('learningApp')
    .controller('foodCtrl', ['$scope', 'food', function($scope, food) {
    
        $scope.foodList = [];
        $scope.loading = true;
        food.list().then(function(data) {
            $scope.loading = false;
            $scope.foodList = data;
        }, function(err) {
            console.error(err); 
        });
        
    }]);
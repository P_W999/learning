angular.module('learningApp')
.filter('foodprice', ['$filter', function($filter) {
    return function(input, size) {
        var result = $filter('filter')(input, function(value, index) {
            return value.name == size;
        });
        if (angular.isDefined(result[0])) {
            return $filter('currency')(result[0].price, '€', 1);
        } else {
            return "";   
        }
    }
}]);
angular.module('learningApp')
.service('food', ['$resource', '$q', 'apiUrl', function ($resource, $q, apiUrl) {
    var foodResource = $resource(apiUrl('food'));
    
    
    this.list = function() {
        var deferred = $q.defer();
        foodResource.get(function(data) {
            deferred.resolve(data['data']);
        }, function(err) {
            deferred.reject(err); 
        });
        return deferred.promise;
    }
}]);
'use strict';

/**
 * @ngdoc overview
 * @name learningApp
 * @description
 * # learningApp
 *
 * Main module of the application.
 */
angular
  .module('learningApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/foodlist', {
        templateUrl: 'views/food.html',
        controller: 'foodCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(function($translateProvider) {
    $translateProvider.translations('en', {
        APP_NAME: 'Hoagie',
        dashboard : 'Dashboard',
        food_list: 'Food List',
        payment_history : 'Deposit history',
        order_history : 'Order history',
        admin_today : 'Today\'s orders',
        admin_userlist : 'User\'s list',
        admin_foodlist : 'Food list',
        about : 'About',
        food_title : 'Available lunches',
        food_name : 'Lunch', 
        food_small : 'S',
        food_medium : 'M',
        food_large : 'L',
        food_smos_small : 'Smos S',
        food_smos_medium : 'Smos M',
        food_smos_large : 'Smos L'
    });
    $translateProvider.preferredLanguage('en');
});


